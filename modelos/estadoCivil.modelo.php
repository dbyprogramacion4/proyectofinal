<?php 
class ModeloEstadoCivil{

static public function mdlSeleccionarEstadosCiviles($tabla)
{

    $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

    $stmt->execute();

    return $stmt -> fetchAll();
		
	$stmt->close();
    $stmt = null;

	}

  /*=============================================
	Eliminar Registro
	=============================================*/
	static public function mdlEliminarEstadosCiviles($tabla, $valor){
	
		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE idEstadoCivil = :id");

		$stmt->bindParam(":id", $valor, PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			print_r(Conexion::conectar()->errorInfo());

		}

		$stmt->close();

		$stmt = null;	

	}
}