<?php 
class ModeloProducto{

static public function mdlSeleccionarProducto($tabla)
{

    $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

    $stmt->execute();

    return $stmt -> fetchAll();
		
		$stmt->close();
    $stmt = null;

	}

  /*=============================================
	Eliminar Registro
	=============================================*/
	static public function mdlEliminarEstadoProducto($tabla, $valor){
	
		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE idProducto = :id");

		$stmt->bindParam(":id", $valor, PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			print_r(Conexion::conectar()->errorInfo());

		}

		$stmt->close();

		$stmt = null;	

	}
}