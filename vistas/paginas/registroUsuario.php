<form method="post" id="formRegistro" enctype="multipart/form-data">

    <div class="form-group">
        <label for="email">Nombre:</label>
        <input type="text" class="form-control" name="nombre">
    </div>

    <div class="form-group">
        <label for="email">Apellido:</label>
        <input type="text" class="form-control" name="apellido">
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" name="email">
    </div>

    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" name="password" id="password">
    </div>

    <div class="form-group">
        <label for="password">Repetir Password:</label>
        <input type="password" class="form-control" name="confirm_password">
    </div>

    <div class="form-group" >

        <select name="idTipoUsuario" class="form-control">

            <option value="">Seleccionar</option>

            <?php $idTipoUsuario = ControladorTipoUsuario::ctrSeleccionarTipoUsuario(); 

            foreach ($idTipoUsuario as $key => $value) { ?>

                <option value="<?php echo $value["idTipoUsuario"] ?>"><?php echo $value["nombre"] ?></option>
            
            <?php } ?>
            
        </select>

    </div>

    <?php

    $crearUsuario = new ControladorUsuarios();
    $crearUsuario -> ctrRegistroUsuario();
    
    ?>
    
    <button type="submit" class="btn btn-primary">Registrar</button>
    
</form>