<form method="post">
    <main class="d-flex flex-column align-items-center">
		<section class = "d-flex flex-column align-items-center my-4 shadow-lg w-75">
			<article class = "d-flex justify-content-center my-4">
				<h2>
					Inicio de sesion
				</h2>
			</article>
			<article>
				<form action="" method="post">
					<div class="input-group mb-5">
						<span class="input-group-text" id="usuario">Usuario</span>
						<input type="text" class="form-control" placeholder="Usuario" aria-label="usuario" aria-describedby="usuario" name="ingresoEmail">
					</div>
					<div class="input-group mb-5">
						<span class="input-group-text" id="contrasenia">Contaseña</span>
						<input type="password" class="form-control" placeholder="Contraseña" aria-label="Pasword" aria-describedby="contrasenia" name="ingresoPassword">
					</div>
                    <?php

                    $ingresoUsuario = new ControladorUsuarios();
                    $ingresoUsuario -> ctrIngresarUsuario();
    
                    ?>
					<div class="d-flex justify-content-evenly mb-4">
						<button class="btn btn-primary" type="submit">Ingresar</button>
					</div>
				</form>
			</article>
		</section>
	</main>

    
</form>