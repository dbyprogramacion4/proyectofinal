<?php

$productos = ControladorProductos::ctrSeleccionarProducto(null, null);

?>

<table class="table table-striped tablaProductos">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>categoria</th> 
            <th>estado</th>
            <th>Acciones</th>					
        </tr>
    </thead>
    <tbody>

    <?php 
    foreach($productos as $value){
    ?>
        <tr>
            <td><?php echo $value["nombre"]; ?></td>
            <td><?php echo $value["idProducto"]; ?></td>
            <td><?php echo $value["estado"]; ?></td>	
            <td><a href="index.php?pagina=editarProducto&tipo=Producto&id=<?php echo $value["idProducto"]; ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
              <button class="btn btn-info btnVerProducto" data-toggle="modal" data-target="#modalVerProducto" idUsuario="<?php echo $value["idUsuario"]; ?>"><i class="fa fa-eye"></i></button>
            </td>
        </tr>
          
        <?php } ?>

    </tbody>
</table>



<!-- Modal -->
<div class="modal fade b-example-mdal" id="modalVerProducto" tabindex="-1" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title apellido" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table table-striped tablaProductos">
    <thead>
        <tr>
            <th>ID producto</th>
            <th>categoria</th>
            <th>Nombre</th>
            <th>Precio</th>					
            <th>imagen</th>
            <th>Estado</th>
            <th>Stock</th>
            <th>Fecha de creación</th>
        </tr>
    </thead>
    <tbody>
      
    <?php 
    foreach($productos as $value){
    ?>
        <tr>
            <td><?php echo $value["idProducto"]; ?></td>
            <td><?php echo $value["idCategoria"]; ?></td>
            <td><?php echo $value["nombre"]; ?></td>
            <td><?php echo $value["precio"]; ?></td>
            <td><?php echo $value["imagen"]; ?></td>
            <td><?php echo $value["estado"]; ?></td>
            <td><?php echo $value["stock"]; ?></td>	
            <td><?php echo $value["fechaCreacion"]; ?></td>	    
        </tr>
          
        <?php } ?>

    </tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>


