<form method="post" id="formRegistroCliente" enctype="multipart/form-data">

    <div class="form-group">
        <label for="nombre">Nombre:</label>
        <input type="text" class="form-control" name="nombre">
    </div>

    <div class="form-group">
        <label for="apellido">Apellido:</label>
        <input type="text" class="form-control" name="apellido">
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" name="email">
    </div>

    <div class="form-group">
        <label for="dni">Dni:</label>
        <input type="number" class="form-control" name="dni">
    </div>

    <div class="form-group">
        <label for="fechaNacimiento">Fecha de Nacimiento:</label>
        <input type="date" class="form-control" name="fechaNacimiento">
    </div>

    <div class="form-group" >

    <select name="idEstadoCivil" class="form-control">

        <option value="">Seleccionar</option>

        <?php $estadoCivil = ControladorEstadoCivil::ctrSeleccionarEstadoCivil(); 

        foreach ($estadoCivil as $key => $value) { ?>

             <option value="<?php echo $value["idEstadoCivil"] ?>"><?php echo $value["nombre"] ?></option>
        
        <?php } ?>
        
    </select>

    </div>

    <?php

    $crearCliente = new ControladorClientes();
    $crearCliente -> ctrRegistroCliente();
    
    ?>
    
    <button type="submit" class="btn btn-primary">Registrar</button>
    
</form>