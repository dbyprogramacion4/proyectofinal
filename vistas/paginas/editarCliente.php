<?php

$item = "idCliente";
$valor = $_GET["id"];

$cliente = ControladorClientes::ctrSeleccionarClientes($item, $valor);

?>

<form method="post">

    <div class="form-group">
        <label for="nombre">Nombre:</label>
        <input type="text" class="form-control" value="<?php echo $cliente["nombre"]; ?>" name="editarNombre">
    </div>

    <div class="form-group">
        <label for="apellido">Apellido:</label>
        <input type="text" class="form-control" value="<?php echo $cliente["apellido"]; ?>" name="editarApellido">
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" value="<?php echo $cliente["email"]; ?>" name="editarEmail">
    </div>

    <div class="form-group">
        <label for="dni">Dni:</label>
        <input type="number" class="form-control" value="<?php echo $cliente["dni"]; ?>" name="editarDni">
    </div>

    <div class="form-group">
        <label for="fechaNacimiento">Fecha de Nacimiento:</label>
        <input type="date" class="form-control" value="<?php echo $cliente["fechaNacimiento"]; ?>" name="editarFechaNacimiento">
    </div>

    <div class="form-group" >

    <select name="idEstadoCivil" class="form-control">

        <option value="">Seleccionar</option>

        <?php $estadoCivil = ControladorEstadoCivil::ctrSeleccionarEstadoCivil(); 

        foreach ($estadoCivil as $key => $value) { ?>

            <option <?php if($cliente["idEstadoCivil"] == $value["idEstadoCivil"]) { ?> selected <?php } ?> value="<?php echo $value["idEstadoCivil"]; ?>"><?php echo $value["nombre"] ?></option>

        <?php } ?>

    </select>
            
    <input type="hidden" value="<?php echo $cliente["idCliente"]; ?>" name="<?php echo "idCliente"; ?>">

    <?php

    $editarCliente = new ControladorClientes();
    $editarCliente -> ctrActualizarCliente();
    
    ?>
    
    <div class="d-flex justify-content-center w-100 mt-5">
        <button type="submit" class="btn btn-primary w-25">Actualizar</button>
    </div>
    
</form>