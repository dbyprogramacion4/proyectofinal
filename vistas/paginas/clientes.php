<?php

$clientes = ControladorClientes::ctrSeleccionarClientes(null, null);

?>
<table class="table table-striped tablaClientes">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Dni</th>
            <th>Acciones</th>					
        </tr>
    </thead>
    <tbody>

    <?php 
    foreach($clientes as $value){
    ?>
        <tr>
            <td><?php echo $value["nombre"]; ?></td>
            <td><?php echo $value["apellido"]; ?></td>
            <td><?php echo $value["dni"]; ?></td>	
            <td><a href="index.php?pagina=editarCliente&tipo=Cliente&id=<?php echo $value["idCliente"]; ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
              <button class="btn btn-info btnVerCliente" data-toggle="modal" data-target="#modalVerCliente" idCliente="<?php echo $value["idCliente"]; ?>"><i class="fa fa-eye"></i></button>
              <button class="btn btn-danger btnEliminarCliente" idEstadoCivil="<?php echo $value["idEstadoCivil"]; ?>" idCliente="<?php echo $value["idCliente"]; ?>"><i class="fa fa-trash"></i></button>
            </td>
        </tr>

        <?php } ?>

    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="modalVerCliente" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title nombreCliente" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <h4 class="email"></h4>
       <img src="" class="previsualizarEditar" width="200" alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<?php

    $eliminarCliente = new ControladorClientes();
    $eliminarCliente -> ctrEliminarCliente();
    
?>