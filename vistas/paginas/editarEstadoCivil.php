<?php

$item = "idEstadoCivil";
$valor = $_GET["id"];

$estadoCivil = ControladorEstadoCivil::ctrSeleccionarEstadosCiviles($item, $valor);

?>

<form method="post">

    <div class="form-group">
        <label for="nombre">Nombre:</label>
        <input type="text" class="form-control" value="<?php echo $estadoCivil["nombre"]; ?>" name="editarNombre">
    </div>
    
    <div class="form-group" >

            

    <?php

    $editarEstadoCivil = new ControladorEstadoCivil();
    $editarEstadoCivil -> ctrActualizarEstadoCivil();
    
    ?>
    
    <div class="d-flex justify-content-center w-100 mt-5">
        <button type="submit" class="btn btn-primary w-25">Actualizar</button>
    </div>
    
</form>