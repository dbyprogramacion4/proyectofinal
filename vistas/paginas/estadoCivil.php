<?php

$estadosCiviles = ControladorEstadoCivil::ctrSeleccionarEstadoCivil(null, null);

?>
<table class="table table-striped tablaEstadoCivil">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Acciones</th>					
        </tr>
    </thead>
    <tbody>

    <?php 
    foreach($estadosCiviles as $value){
    ?>
        <tr>
            <td><?php echo $value["nombre"]; ?></td>
            <td><a href="index.php?pagina=editarEstadoCivil&tipo=EstadoCivil&id=<?php echo $value["idEstadoCivil"]; ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
              <button class="btn btn-info btnVerEstadoCivil" data-toggle="modal" data-target="#modalVerEstadoCivil" idEstadoCivil="<?php echo $value["idEstadoCivil"]; ?>"><i class="fa fa-eye"></i></button>
              <button class="btn btn-danger btnEliminarEstadoCivil" idEstadoCivil="<?php echo $value["idEstadoCivil"]; ?>"><i class="fa fa-trash"></i></button>
            </td>
        </tr>

        <?php } ?>

    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="modalVerEstadoCivil" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title nombreEstadoCivil" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <h4 class="email"></h4>
       <img src="" class="previsualizarEditar" width="200" alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<?php

    $eliminarEstadoCivil = new ControladorEstadoCivil();
    $eliminarEstadoCivil -> ctrEliminarEstadoCivil();
    
?>