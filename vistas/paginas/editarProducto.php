<?php

$item = "idProducto";
$valor = $_GET["id"];

$producto = ControladorProductos::ctrSeleccionarProducto($item, $valor);

print_r($valor);

?>

<form method="post">

    <div class="form-group">
        <label for="nombre">Nombre:</label>
        <input type="text" class="form-control" value="<?php echo $producto["fechaCreacion"]; ?>" name="editarNombre">
    </div>
    <div class="form-group">
        <label for="precio">Precio:</label>
        <input type="text" class="form-control" value="<?php echo $producto["precio"]; ?>" name="editarNombre">
    </div>
    <div class="form-group">
        <label for="stock">Stock:</label>
        <input type="text" class="form-control" value="<?php echo $producto["stock"]; ?>" name="editarNombre">
    </div>
    <div class="form-group">
        <label for="estado">Estado:</label>
        <input type="text" class="form-control" value="<?php echo $producto["estado"]; ?>" name="editarNombre">
    </div>

    <div class="form-group" >

    <select name="tipo" class="form-control">

        <option value="">Seleccionar</option>

        <?php $tipo = ControladorTipo::ctrSeleccionarTipo(); 

        foreach ($tipo as $key => $value) { ?>

            <option <?php if($usuario["tipo"] == $value["idTipo"]) { ?> selected <?php } ?> value="<?php echo $value["idTipo"]; ?>"><?php echo $value["nombre"] ?></option>

        <?php } ?>

        </select>

    <?php

    $editarProducto = new ControladorProductos();
    $editarUsuario -> ctrActualizarProducto();
    
    ?>
    
    <button type="submit" class="btn btn-primary">Actualizar</button>
    
</form>