<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Ejemplo MVC</title>

	<!--=====================================
	PLUGINS DE CSS
	======================================-->	

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- plugin css -->
    <link href="assets/libs/jsvectormap/css/jsvectormap.min.css" rel="stylesheet" type="text/css" />
    <!-- swiper css -->
    <link rel="stylesheet" href="assets/libs/swiper/swiper-bundle.min.css">
    <!-- Bootstrap Css -->
    <link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Css -->
    <link href="assets/css/styles.css" rel="stylesheet" type="text/css" />

	<!--=====================================
	PLUGINS DE JS
	======================================-->	

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<!-- Latest compiled Fontawesome-->
	<script src="https://kit.fontawesome.com/e632f1f723.js" crossorigin="anonymous"></script>

	<!-- Latest sweetalert2-->
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
	

</head>
<body data-layout="horizontal" data-topbar="dark">
    <div id="layout-wrapper">
        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box">
                        <a href="index.html" class="logo logo-dark">
                            <span class="logo-sm">
                                <img src="assets/images/logo-sm.svg" alt="" height="26">
                            </span>
                            <span class="logo-lg">
                                <img src="assets/images/logo-sm.svg" alt="" height="26"> <span class="logo-txt">Vuesy</span>
                            </span>
                        </a>

                        <a href="index.html" class="logo logo-light">
                            <span class="logo-sm">
                                <img src="assets/images/logo-sm.svg" alt="" height="26">
                            </span>
                            <span class="logo-lg">
                                <img src="assets/images/logo-sm.svg" alt="" height="26"> <span class="logo-txt">Vuesy</span>
                            </span>
                        </a>
                    </div>

                    <div class="topnav">
                        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">
        
                            <div class="navbar">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link " href="#"  role="button">
                                            <i class="bx bx-home-circle icon"></i>
                                            <span data-key="t-dashboard">Inicio</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="dropdown me-5">
                                <button class="btn button dropdown-toggle" type="button" id="dropdowUsuarios" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Usuario
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdowUsuarios">
                                    <li><a class="dropdown-item" href="index.php?pagina=usuarios">Usuario</a></li>
                                    <li><a class="dropdown-item" href="index.php?pagina=registroUsuario">Crear Usuario</a></li>
                                    <li><a class="dropdown-item" href="index.php?pagina=tipoUsuario">Tipo de usuario</a></li>
                                    <li><a class="dropdown-item" href="index.php?pagina=registroTipoUsuario">Crear tipo de usuario</a></li>
                                </div>
                            </div>
                            <div class="dropdown me-5">
                                <button class="btn button dropdown-toggle" type="button" id="dropdowProductos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Producto
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdowProductos">
                                    <li><a class="dropdown-item" href="index.php?pagina=productos">producto</a></li>
                                </div>
                            </div>
                            <div class="dropdown me-5">
                                <button class="btn button dropdown-toggle" type="button" id="dropdowClientes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Cliente
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdowClientes">
                                    <li><a class="dropdown-item" href="index.php?pagina=clientes">Cliente</a></li>
                                    <li><a class="dropdown-item" href="index.php?pagina=registroCliente">Crear cliente</a></li>
                                    <li><a class="dropdown-item" href="index.php?pagina=estadoCivil">Estado civil</a></li>
                                </div>
                            </div>
							<div class="navbar" >
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link " href="index.php?pagina=salir"  role="button">
                                            <span data-key="t-dashboard">Salir</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>

                </div>

                <div class="d-flex">
                    <div class="d-inline-block">
                        <a type="button" class="btn header-item user text-start d-flex align-items-center" <?php if(!isset($_GET["pagina"]) || $_GET["pagina"] == 'salir'){echo 'href = "index.php?pagina=ingreso"';}?> >
                            <img class="rounded-circle header-profile-user" src="assets/images/users/usuario.png"
                            alt="Header Avatar">
                            <span class="ms-2 d-none d-xl-inline-block user-item-desc">
                                <span class="user-name">
                                    <?php 
                                        if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
                                            $usuarioLogueado = ControladorUsuarios::ctrSeleccionarUsuarios("idUsuario", $_SESSION["idUsuario"]);
                                            echo $usuarioLogueado['nombre'];
                                        }else{
                                        echo 'Ingresar';
                                        }?>
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </header>
        
        <footer class="footer mt-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> &copy; copyright
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            Desarrollado Por:  Matias, Ezequiel y Patricio
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

	<?php if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){ ?>
        <div class="container-fluid">
            <div class="container py-5">
                <div class="row">
                    <div class="col col-md-12">
                        <?php
                            $usuarioLogueado = ControladorUsuarios::ctrSeleccionarUsuarios("idUsuario", $_SESSION["idUsuario"]);
                        ?>
                    </div>
                </div>
                <?php 
                    if(isset($_GET["pagina"])){
                        if($_GET["pagina"] == "usuarios" ||

                            $_GET["pagina"] == "registroUsuario" ||

                            $_GET["pagina"] == "registro" ||
                            $_GET["pagina"] == "registroCliente" ||

                            $_GET["pagina"] == "editar" ||
                            $_GET["pagina"] == "editarProducto" ||
                            $_GET["pagina"] == "productos"||
                            $_GET["pagina"] == "editarCliente" ||
                            $_GET["pagina"] == "editarEstadoCivil" ||
                            $_GET["pagina"] == "salir" ||
                            $_GET["pagina"] == "ingreso"||
                            $_GET["pagina"] == "tipoUsuario"||

                            $_GET["pagina"] == "registroTipoUsuario"||

                            $_GET["pagina"] == "clientes"

                        ){
                            include "paginas/".$_GET["pagina"].".php";
                        }
                    }else{
                        include "paginas/ingreso.php";
                    }
                ?>
            </div>
        </div>

	<?php }else { ?>

		<div class="container-fluid">
		    <div class="container py-5">
		        <?php include "paginas/ingreso.php"; ?>
		    </div>
		</div>
	<?php } ?>

	<script src="vistas/js/validar.js"></script>
	<script src="vistas/js/usuarios.js"></script>
	<script src="vistas/js/tiposUsuario.js"></script>
	<script src="vistas/js/clientes.js"></script>
	<script src="vistas/js/estadoCivil.js"></script>

</body>
</html>