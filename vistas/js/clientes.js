$(".tablaClientes tbody").on("click", ".btnEliminarCliente", function(){

    var idCliente = $(this).attr("idCliente");
    var idEstadoCivil = $(this).attr("idEstadoCivil");

    Swal.fire({
        title: 'Quiere eliminar el cliente?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "index.php?pagina=clientes&idEliminarCliente="+idCliente;
        }
      })

})

//traer datos del cliente
$(".tablaClientes").on("click", ".btnVerCliente", function(){

	var idCliente = $(this).attr("idCliente");
	
	var datos = new FormData();
	datos.append("idCliente", idCliente);

	$.ajax({

		url:"ajax/clientes.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

      console.log(respuesta);
			
			$(".nombreCliente").html(respuesta["nombre"]);
		}

	});

})