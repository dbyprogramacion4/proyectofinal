$(".tablaTipoUsuario tbody").on("click", ".btnEliminarTipoUsuario", function(){

    var idTipoUsuario = $(this).attr("idTipoUsuario");

    if(idTipoUsuario == 1)
    {
      Swal.fire({
					
        icon: "error",
        title: "No se puede eliminar el tipo de usuario super administrador",
        showConfirmButton: true,
        confirmButtonText:"Cerrar"
        });

    }else{

    Swal.fire({
        title: 'Quiere eliminar el tipo de usuario?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "index.php?pagina=usuarios&idEliminarUsuario="+idTipoUsuario;
        }
      })

    }

})

// //traer datos del usuario
// $(".tablaTipoUsuario").on("click", ".btnVerUsuario", function(){

// 	var idUsuario = $(this).attr("idUsuario");
	
// 	var datos = new FormData();
// 	datos.append("idUsuario", idUsuario);

// 	$.ajax({

// 		url:"ajax/usuarios.ajax.php",
// 		method: "POST",
// 		data: datos,
// 		cache: false,
// 		contentType: false,
// 		processData: false,
// 		dataType: "json",
// 		success: function(respuesta){

//       console.log(respuesta);
			
// 			$(".nombreUsuario").html(respuesta["nombre"]);
// 			$(".email").html(respuesta["email"]);			

// 		}

// 	});

// })