$(".tablaProductos tbody").on("click", ".btnEliminarUsuario", function(){

    var idProducto = $(this).attr("idProducto");
   

    if(idProducto == 1)
    {
      Swal.fire({
					
        icon: "error",
        title: "No se puede eliminar un administrador",
        showConfirmButton: true,
        confirmButtonText:"Cerrar"
        });

    }else{

    Swal.fire({
        title: 'Quiere eliminar el usuario?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "index.php?pagina=usuarios&idEliminarUsuario="+idProducto;
        }
      })

    }

})

//traer datos del usuario
$(".tablaProductos").on("click", ".btnVerProducto", function(){

	var idProducto = $(this).attr("idProducto");
	
	var datos = new FormData();
	datos.append("idProducto", idProducto);

	$.ajax({

		url:"ajax/productos.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

      console.log(respuesta);
			
			$(".nombre").html(respuesta["nombre"]);
			$(".precio").html(respuesta["precio"]);			
      $(".Categoria").html(respuesta["idCategoria"]);

		}

	});

})