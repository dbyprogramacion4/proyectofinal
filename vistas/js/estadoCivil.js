$(".tablaEstadoCivil tbody").on("click", ".btnEliminarEstadoCivil", function(){

    var idEstadoCivil = $(this).attr("idEstadoCivil");

    Swal.fire({
        title: 'Quiere eliminar el estado civil?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "index.php?pagina=estadoCivil&idEliminarEstadoCivil="+idEstadoCivil;
        }
      })

})

//traer datos del estado civil
$(".tablaEstadoCivil").on("click", ".btnVerEstadoCivil", function(){

	var idEstadoCivil = $(this).attr("idEstadoCivil");
	
	var datos = new FormData();
	datos.append("idEstadoCivil", idEstadoCivil);
})