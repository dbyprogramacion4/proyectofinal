<?php

require_once "../controladores/clientes.controlador.php";
require_once "../modelos/clientes.modelo.php";

class AjaxVerCliente{

/*=============================================
TRAER CLIENTE
=============================================*/	

	public $idCliente;

	public function ajaxTraerCliente(){

		$item = "idCliente";
		$valor = $this->idCliente;	

		$respuesta = ControladorClientes::ctrSeleccionarClientes($item, $valor);

		echo json_encode($respuesta);

	}

}


if(isset($_POST["idCliente"])){

	$traeCliente = new AjaxVerCliente();
	$traeCliente -> idCliente = $_POST["idCliente"];
	$traeCliente -> ajaxTraerCliente();

}