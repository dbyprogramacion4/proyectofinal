<?php

require_once "../controladores/estadoCivil.controlador.php";
require_once "../modelos/estadoCivil.modelo.php";

class AjaxVerestadoCivil{

/*=============================================
TRAER Estado Civil
=============================================*/	

	public $idEstadoCivil;

	public function ajaxTraerEstadoCivil(){

		$item = "idEstadoCivil";
		$valor = $this->idEstadoCivil;	

		$respuesta = ControladorEstadoCivil::ctrSeleccionarEstadosCiviles($item, $valor);

		echo json_encode($respuesta);

	}

}


if(isset($_POST["idEstadoCivil"])){

	$traeEstadoCivil = new AjaxVerEstadoCivil();
	$traeEstadoCivil -> idEstadoCivil = $_POST["idEstadoCivil"];
	$traeEstadoCivil -> ajaxTraerEstadoCivil();

}