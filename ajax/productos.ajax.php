<?php

require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

class AjaxVerProducto{

/*=============================================
TRAER USuario
=============================================*/	

	public $idProducto;

	public function ajaxTraerProducto(){

		$item = "idProducto";
		$valor = $this->idProducto;	

		$respuesta = ControladorProductos::ctrSeleccionarProducto($item, $valor);

		echo json_encode($respuesta);

	}

}

/*=============================================
TRAER PRODUCTO
=============================================*/
if(isset($_POST["idProducto"])){

	$traeProducto = new AjaxVerProducto();
	$traeProducto -> idProducto = $_POST["idProducto"];
	$traeProducto -> ajaxTraerProducto();

}