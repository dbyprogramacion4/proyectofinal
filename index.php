<?php

require_once './controladores/plantilla.controlador.php';

require_once './controladores/usuarios.controlador.php';
require_once './controladores/productos.controlador.php';
require_once './controladores/tipoUsuario.controlador.php';
require_once './controladores/clientes.controlador.php';
require_once './controladores/estadoCivil.controlador.php';

require_once './modelos/usuarios.modelo.php';
require_once './modelos/productos.modelo.php';
require_once './modelos/tipoUsuario.modelo.php';
require_once './modelos/clientes.modelo.php';
require_once './modelos/estadoCivil.modelo.php';

$plantilla = new ControladorPlantilla();
$plantilla -> ctrMostrarPlantilla();
