<?php

class ControladorClientes{

    /*=============================================
	Seleccionar Registros
	=============================================*/

	static public function ctrSeleccionarClientes($item, $valor){

		$tabla = "cliente";

		$respuesta = ModeloClientes::mdlSeleccionarClientes($tabla, $item, $valor);

		return $respuesta;

	}
/*=============================================
	Registro
	=============================================*/

	static public function ctrRegistroCliente(){

		if(isset($_POST["nombre"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombre"]))
			{
			 //validar no existe email

			$item = "email";
			$valor = $_POST["email"];

			$cliente = ControladorClientes::ctrSeleccionarClientes($item, $valor);
		
			if(!$cliente){

			$tabla = "cliente";

			$datos = array("nombre" => $_POST["nombre"],
						   "apellido" => $_POST["nombre"],
				           "email" => $_POST["email"],
						   "dni" => $_POST["dni"],
						   "fechaNacimiento" => $_POST["fechaNacimiento"],
						   "idEstadoCivil" => $_POST["idEstadoCivil"]);

						 

			$respuesta = ModeloClientes::mdlRegistroCliente($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=clientes";

				</script>';

			}
		}else{

			echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "El email ya existe en la BD",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				  });

				</script>';


		}
		}else{

			echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "No se permiten caracteres especiales en el nombre",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				  });

				</script>';

		}

		}

	}

/*=============================================
	Editar
	=============================================*/

	static public function ctrActualizarCliente(){

		if(isset($_POST["editarNombre"])){

			$tabla = "cliente";

			$datos = array("idCliente" => $_POST["idCliente"],
						   "nombre" => $_POST["editarNombre"],
						   "apellido" => $_POST["editarApellido"],
				           "email" => $_POST["editarEmail"],
				           "dni" => $_POST["editarDni"],
				           "fechaNacimiento" => $_POST["editarFechaNacimiento"],
						   "idEstadoCivil" => $_POST["idEstadoCivil"]);

			$respuesta = ModeloClientes::mdlActualizarCliente($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=clientes";

				</script>';

			}

			return $respuesta;


		}


	}

/*=============================================
	Eliminar Registro
	=============================================*/
	public function ctrEliminarCliente(){

		if(isset($_GET["idEliminarCliente"])){

			$tabla = "cliente";
			$valor = $_GET["idEliminarCliente"];

			$respuesta = ModeloClientes::mdlEliminarClientes($tabla, $valor);

			if($respuesta == "ok"){

				echo '<script>

				Swal.fire({
					
					icon: "success",
					title: "El cliente se eliminó correctamente",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=clientes";
					}
				  });

				</script>';

			}

		}

	}
}