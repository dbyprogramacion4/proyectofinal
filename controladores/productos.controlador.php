<?php

class ControladorProductos{

	/*=============================================
	Seleccionar Registros
	=============================================*/

	static public function ctrSeleccionarProducto($item, $valor){

		$tabla = "producto";

		$respuesta = ModeloProducto::mdlSeleccionarProducto($tabla, $item, $valor);

		return $respuesta;

	}

/*=============================================
	Actualizar Registro
	=============================================*/

	static public function ctrActualizarProducto(){

		if(isset($_POST["editarNombre"])){

			$tabla = "producto";

			$datos = array("idProducto" => $_POST["idProducto"],
						   "nombre" => $_POST["editarNombre"],
						   "apellido" => $_POST["editarApellido"],
				           "email" => $_POST["editarEmail"],
				           "idTipoUsuario" => $_POST["idTipoUsuario"],
				           "password" => $password);

			$respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=productos";

				</script>';

			}

			return $respuesta;


		}


	}

	/*=============================================
	Eliminar Registro
	=============================================*/
	public function ctrEliminarEstadoProducto(){

		if(isset($_GET["idEliminarEstadoProducto"])){

			$tabla = "tipoUsuario";
			$valor = $_GET["idEliminarEstadoProducto"];

			$respuesta = ModeloProducto::mdlEliminarEstadoProducto($tabla, $valor);

			if($respuesta == "ok"){

				echo '<script>

				Swal.fire({
					
					icon: "success",
					title: "El tipo de usuario se eliminó correctamente",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=usuarios";
					}
				  });

				</script>';

			}

		}

	}
}