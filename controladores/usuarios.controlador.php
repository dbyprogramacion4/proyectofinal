<?php

class ControladorUsuarios{

	/*=============================================
	Login de usuario
	=============================================*/
	static public function ctrIngresarUsuario(){

		if(isset($_POST["ingresoEmail"])){

			$encriptar = crypt($_POST["ingresoPassword"], '$2a$07$gongkosiosefopenguolgphst$');
			$tabla = "usuario";

			$item = "email";
			$valor = $_POST["ingresoEmail"];

			$respuesta = ModeloUsuarios::mdlSeleccionarUsuarios($tabla, $item, $valor);

			if(is_array($respuesta) && $_POST["ingresoEmail"] == $respuesta["email"] && $encriptar == $respuesta["password"]){
				
				$_SESSION["iniciarSesion"] = "ok";
				$_SESSION["idUsuario"] = $respuesta["idUsuario"];
				$_SESSION["idTipoUsuario"] = $respuesta["idTipoUsuario"];

				echo '<script>			
					
					
					window.location = "index.php?pagina=usuarios";
					

				</script>';

			}else
			{
				echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "Usuario o contraseña inválidos",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php";
					}
				  });

				</script>';
			}


		}//else{



		// }

	}

    /*=============================================
	Seleccionar Registros
	=============================================*/

	static public function ctrSeleccionarUsuarios($item, $valor){

		$tabla = "usuario";

		$respuesta = ModeloUsuarios::mdlSeleccionarUsuarios($tabla, $item, $valor);

		return $respuesta;

	}
	/*=============================================
	Registro
	=============================================*/

	static public function ctrRegistroUsuario(){

		if(isset($_POST["nombre"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombre"]))
			{
			 //validar no existe email

			$item = "email";
			$valor = $_POST["email"];

			$usuario = ControladorUsuarios::ctrSeleccionarUsuarios($item, $valor);
		
			if(!$usuario){

			$password = crypt($_POST["password"], '$2a$07$gongkosiosefopenguolgphst$');

			$tabla = "usuario";

			$datos = array("nombre" => $_POST["nombre"],
						   "apellido" => $_POST["nombre"],
				           "email" => $_POST["email"],
						   "idTipoUsuario" => $_POST["idTipoUsuario"],
				           "password" => $password,
						   "estado" => 1);

						 

			$respuesta = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=usuarios";

				</script>';

			}
		}else{

			echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "El email ya existe en la BD",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				  });

				</script>';


		}
		}else{

			echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "No se permiten caracteres especiales en el nombre",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				  });

				</script>';

		}

		}

	}

	/*=============================================
	Editar
	=============================================*/

	static public function ctrActualizarUsuario(){

		if(isset($_POST["editarNombre"])){

			if($_POST["editarPassword"] != ""){			

				$password = crypt($_POST["editarPassword"], '$2a$07$gongkosiosefopenguolgphst$');

			}else{

				$item = "idUsuario";
				$valor = $_POST["idUsuario"];

				$usuario = ControladorUsuarios::ctrSeleccionarUsuarios($item, $valor);

				$password = $usuario["password"];
			}


			$tabla = "usuario";

			$datos = array("idUsuario" => $_POST["idUsuario"],
						   "nombre" => $_POST["editarNombre"],
						   "apellido" => $_POST["editarApellido"],
				           "email" => $_POST["editarEmail"],
				           "idTipoUsuario" => $_POST["idTipoUsuario"],
				           "password" => $password);

			$respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=usuarios";

				</script>';

			}

			return $respuesta;


		}


	}

	/*=============================================
	Eliminar Registro
	=============================================*/
	public function ctrEliminarUsuario(){

		if(isset($_GET["idEliminarUsuario"])){

			$tabla = "usuario";
			$valor = $_GET["idEliminarUsuario"];

			$respuesta = ModeloUsuarios::mdlEliminarUsuarios($tabla, $valor);

			if($respuesta == "ok"){

				echo '<script>

				Swal.fire({
					
					icon: "success",
					title: "El usuario se eliminó correctamente",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=usuarios";
					}
				  });

				</script>';

			}

		}

	}
}