<?php

class ControladorEstadoCivil{


	static public function ctrSeleccionarEstadoCivil(){

		$tabla = "estadocivil";

		$respuesta = ModeloEstadoCivil::mdlSeleccionarEstadosCiviles($tabla);

		return $respuesta;

	}

    /*=============================================
	Seleccionar Registros
	=============================================*/

	static public function ctrSeleccionarEstadosCiviles($item, $valor){

		$tabla = "estadocivil";

		$respuesta = ModeloEstadoCivil::mdlSeleccionarEstadosCiviles($tabla, $item, $valor);

		return $respuesta;

	}
/*=============================================
	Registro
	=============================================*/

	static public function ctrRegistroEstadoCivil(){

		if(isset($_POST["nombre"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombre"]))
			{
			 //validar no existe nombre

			$item = "nombre";
			$valor = $_POST["nombre"];

			$nombre = ControladorEstadoCivil::ctrSeleccionarEstadosCiviles($item, $valor);
		
			if(!$nombre){

			$tabla = "estadocivil";

			$datos = array("nombre" => $_POST["nombre"]);

						 

			$respuesta = ModeloEstadoCivil::mdlRegistroEstadoCivil($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=estadoCivil";

				</script>';

			}
		}else{

			echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "El nombre ya existe en la BD",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				  });

				</script>';


		}
		}else{

			echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "No se permiten caracteres especiales en el nombre",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				  });

				</script>';

		}

		}

	}

/*=============================================
	Editar
	=============================================*/

	static public function ctrActualizarEstadoCivil(){

		if(isset($_POST["editarNombre"])){

			$item = "idEstadoCivil";
			$valor = $_POST["idEstadoCivil"];

			$estadoCivil = ControladorEstadoCivil::ctrSeleccionarEstadosCiviles($item, $valor);


			$tabla = "estadocivil";

			$datos = array("nombre" => $_POST["editarNombre"]);

			$respuesta = ModeloEstadoCivil::mdlActualizarEstadoCivil($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=estadoCivil";

				</script>';

			}

			return $respuesta;


		}


	}

/*=============================================
	Eliminar Registro
	=============================================*/
	public function ctrEliminarEstadoCivil(){

		if(isset($_GET["idEliminarEstadoCivil"])){

			$tabla = "estadocivil";
			$valor = $_GET["idEliminarEstadoCivil"];

			$respuesta = ModeloEstadoCivil::mdlEliminarEstadosCiviles($tabla, $valor);

			if($respuesta == "ok"){

				echo '<script>

				Swal.fire({
					
					icon: "success",
					title: "El estado civil se eliminó correctamente",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=estadoCivil";
					}
				  });

				</script>';

			}

		}

	}
}