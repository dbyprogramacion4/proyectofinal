<?php

class ControladorTipoUsuario{

    static public function ctrSeleccionarTipoUsuario(){

		$tabla = "tipoUsuario";

		$respuesta = ModeloTipoUsuario::mdlSeleccionarTiposUsuario($tabla);

		return $respuesta;

	}

	/*=============================================
	Seleccionar Registros
	=============================================*/

	static public function ctrSeleccionarTiposUsuario($item, $valor){

		$tabla = "tipoUsuario";

		$respuesta = ModeloTipoUsuario::mdlSeleccionarTiposUsuario($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	Registro
	=============================================*/

	static public function ctrRegistroTiposUsuario(){

		if(isset($_POST["nombre"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombre"]))
			{
			 //validar no existe email

			$item = "nombre";
			$valor = $_POST["nombre"];

			$tipoUsuario = ControladorTipoUsuario::ctrSeleccionarTipoUsuario($item, $valor);
		
			if(!$tipoUsuario){

			$tabla = "tipoUsuario";

			$datos = array("nombre" => $_POST["nombre"]);

						 

			$respuesta = ModeloTipoUsuario::mdlRegistroTipoUsuario($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=usuarios";

				</script>';

			}
		}else{

			echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "El email ya existe en la BD",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				  });

				</script>';


		}
		}else{

			echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "No se permiten caracteres especiales en el nombre",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				  });

				</script>';

		}

		}

	}

	/*=============================================
	Eliminar Registro
	=============================================*/
	public function ctrEliminarTipoUsuario(){

		if(isset($_GET["idEliminarTipoUsuario"])){

			$tabla = "tipoUsuario";
			$valor = $_GET["idEliminarTipoUsuario"];

			$respuesta = ModeloTiposUsuario::mdlEliminarTiposUsuario($tabla, $valor);

			if($respuesta == "ok"){

				echo '<script>

				Swal.fire({
					
					icon: "success",
					title: "El tipo de usuario se eliminó correctamente",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=usuarios";
					}
				  });

				</script>';

			}

		}

	}
}