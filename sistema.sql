/*----------------Borrar esto antes de crear las tablas------------------*/

/*DROP DATABASE proyectoFinal;*/

/*CREATE DATABASE proyectoFinal;*/

/*--------------------------------Tipos-usuario-----------------------------------*/

/*DROP TABLE IF EXISTS `tipoUsuario`;*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `tipoUsuario` (
  `idTipoUsuario` int(11) NOT NULL,
  `nombre` text DEFAULT NULL,
  `fechaCreacion` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT INTO `tipoUsuario` (`idTipoUsuario`, `nombre`, `fechaCreacion`) VALUES
(1, 'Super administrador', '2022-11-17 03:37:47');

ALTER TABLE `tipoUsuario`
ADD PRIMARY KEY (`idTipoUsuario`),
MODIFY `idTipoUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*------------------------------Usuarios------------------------------------------*/

/*DROP TABLE IF EXISTS `usuario`;*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `idTipoUsuario` int(11) NOT NULL,
  `nombre` text DEFAULT NULL,
  `apellido` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `fechaCreacion` timestamp NULL DEFAULT current_timestamp(),
  `ultimoIngreso` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `usuario` (`idUsuario`, `nombre`, `apellido`, `email`, `idTipoUsuario`, `password`, `estado`, `fechaCreacion`, `ultimoIngreso`) VALUES
(1, 'Administrador', 'Admin', 'admin@hotmail.com', 1, '$2a$07$gongkosiosefopenguolgeLSi5po.jqJuXW0zJWsx/4owq5KNKRTa', 1, '2022-11-17 03:37:47', '2022-11-17 03:37:47');

ALTER TABLE `usuario`
ADD PRIMARY KEY (`idUsuario`),
ADD FOREIGN KEY(`idTipoUsuario`) REFERENCES `tipoUsuario`(`idTipoUsuario`),
MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*-------------------------Estado Civil-------------------------------*/

/*DROP TABLE IF EXISTS `estadoCivil`;*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `estadoCivil` (
  `idEstadoCivil` int(0) NOT NULL AUTO_INCREMENT,
  `nombre` text DEFAULT NULL,
  `fechaCreacion` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idEstadoCivil`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

/*--------------------------Clientes-------------------------------*/

/*DROP TABLE IF EXISTS `cliente`;*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `cliente` (
  `idCliente` int(0) NOT NULL AUTO_INCREMENT,
  `idEstadoCivil` int(0) NOT NULL,
  `nombre` text DEFAULT NULL,
  `apellido` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `dni` int(0) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `fechaCreacion` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

ALTER TABLE `cliente`
ADD FOREIGN KEY(`idEstadoCivil`) REFERENCES `estadoCivil`(`idEstadoCivil`);

/*-------------------------Categoría-------------------------------*/

/*DROP TABLE IF EXISTS `categoria`;*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `categoria` (
  `idCategoria` int(0) NOT NULL AUTO_INCREMENT,
  `nombre` text DEFAULT NULL,
  `fechaCreacion` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

-------------------------Producto-------------------------------


/*DROP TABLE IF EXISTS `producto`;*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL AUTO_INCREMENT,
  `idCategoria` int(11) NOT NULL,
  `nombre` text DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `imagen` text DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `fechaCreacion` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

ALTER TABLE `producto`
ADD FOREIGN KEY(`idCategoria`) REFERENCES `categoria`(`idCategoria`);
